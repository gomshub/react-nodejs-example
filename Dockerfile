FROM node:18.13.0 AS ui-build
WORKDIR /usr/src/app
RUN pwd
COPY my-app/ ./my-app/
RUN cd my-app && npm install -g npm@latest && npm i react-scripts && npm run build

FROM node:18.13.0 AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/my-app/build ./my-app/build
COPY api/package*.json ./api/
COPY api/server.js ./api/
RUN cd api && npm install

EXPOSE 3080

CMD ["node", "./api/server.js"]
